function CrearCliente(datosEntrantes, respuesta) {
    // en primer lugar se convierte el string de los datos que recibe un json con el cual se puede utilizar
    var entradaString = JSON.stringify(datosEntrada.body);
    var ingreso = JSON.parse(entradaString);
    // Ahora vamos a crear un sql con los datos del cliente que vamos a ingresar el cual vamos a usar para hacer el ingreso en la base de datos
    const consulta = `insert 
      into clientes (cedula, apellidos, nombres, direccion, telefono)
      values('${ingreso['cedula']}', '${ingreso['apellidos']}', '${ingreso['nombres']}', '${ingreso['direccion']}', '${ingreso['telefono']}');`
      try {
        database.query(consulta).then((datos) => {
            return respuesta.status(200).json({ "mensaje": "Se ha creado el usuario" });
        })
    } catch (error) {
        if (error.routine === '_bt_check_unique') {
            return respuesta.status(400).send({ 'mensaje': 'Error en el proceso de crear usuario' })
        }
        return respuesta.status(400).send(error);
    }
}
}
